# $@ : Fait référence à la cible
# $^ : Fait référence à la dépendance

bin/affichage_table: obj/affichage.o obj/calcul.o obj/main.o
	g++ $(shell pkg-config --libs libpq) -o $@ $^

obj/main.o: src/main.cpp
	g++ -c $(shell pkg-config --cflags libpq) -o $@ $^

obj/calcul.o: src/calcul.cpp
	g++ -c $(shell pkg-config --cflags libpq) -o $@ $^

obj/affichage.o: src/affichage.cpp
	g++ -c $(shell pkg-config --cflags libpq) -o $@ $^

#Nettoyage
clean:
	-rm bin/* obj/*.o

#Ajout des modifications sur Git
add:
	git add *

#Afficher le status de Git
status:
	git status

#Push sur Git
push:
	git push origin master
